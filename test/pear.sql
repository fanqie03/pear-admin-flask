/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.10.179
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : 192.168.10.179:3306
 Source Schema         : PearAdminFlask

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 16/11/2022 14:25:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_admin_log`;
CREATE TABLE `admin_admin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `uid` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `success` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2211 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for admin_dept
-- ----------------------------
DROP TABLE IF EXISTS `admin_dept`;
CREATE TABLE `admin_dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级编号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '部门名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `leader` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系方式',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态(1开启,0关闭)',
  `remark` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '备注',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '详细地址',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_dept
-- ----------------------------
INSERT INTO `admin_dept` VALUES (1, 0, '总公司', 1, '就眠仪式', '12312345679', '123qq.com', 1, NULL, '这是总公司', NULL, '2021-06-01 17:23:20');
INSERT INTO `admin_dept` VALUES (4, 1, '济南分公司', 2, '就眠仪式', '12312345678', '1234qq.com', 1, NULL, '这是济南', '2021-06-01 17:24:33', '2021-06-01 17:25:19');
INSERT INTO `admin_dept` VALUES (5, 1, '唐山分公司', 4, 'mkg', '12312345678', '123@qq.com', 1, NULL, '这是唐山', '2021-06-01 17:25:15', '2021-06-01 17:25:20');
INSERT INTO `admin_dept` VALUES (7, 4, '济南分公司开发部', 5, '就眠仪式', '12312345678', '123@qq.com', 1, NULL, '测试', '2021-06-01 17:27:39', '2021-06-01 17:27:39');
INSERT INTO `admin_dept` VALUES (8, 5, '唐山测试部', 6, 'mkg', '12312345678', '123@qq.com', 1, NULL, '测试部', '2021-06-01 17:28:27', '2021-06-01 17:28:27');

-- ----------------------------
-- Table structure for admin_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `admin_dict_data`;
CREATE TABLE `admin_dict_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典类型名称',
  `data_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典类型标识',
  `type_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典类型描述',
  `is_default` int(11) NULL DEFAULT NULL COMMENT '是否默认',
  `enable` int(11) NULL DEFAULT NULL COMMENT '是否开启',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_dict_data
-- ----------------------------
INSERT INTO `admin_dict_data` VALUES (8, '男', 'boy', 'user_sex', NULL, 1, '男 : body', '2021-04-16 13:36:34', '2021-04-16 14:05:06');
INSERT INTO `admin_dict_data` VALUES (9, '女', 'girl', 'user_sex', NULL, 1, '女 : girl', '2021-04-16 13:36:55', '2021-04-16 13:36:55');

-- ----------------------------
-- Table structure for admin_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `admin_dict_type`;
CREATE TABLE `admin_dict_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典类型名称',
  `type_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典类型标识',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典类型描述',
  `enable` int(11) NULL DEFAULT NULL COMMENT '是否开启',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_dict_type
-- ----------------------------
INSERT INTO `admin_dict_type` VALUES (1, '用户性别', 'user_sex', '用户性别', 1, NULL, '2021-04-16 13:37:11');

-- ----------------------------
-- Table structure for admin_mail
-- ----------------------------
DROP TABLE IF EXISTS `admin_mail`;
CREATE TABLE `admin_mail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '邮件编号',
  `receiver` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收件人邮箱',
  `subject` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮件主题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '邮件正文',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '发送人id',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_mail
-- ----------------------------
INSERT INTO `admin_mail` VALUES (1, '1242733702@qq.com', 'pear-admin-flask', 'pear-admin-flask', 1, '2022-10-11 13:41:21');
INSERT INTO `admin_mail` VALUES (4, '1242733702@qq.com', '湖人总冠军', '湖人总冠军', 1, '2022-10-11 14:03:30');
INSERT INTO `admin_mail` VALUES (5, '1242733702@qq.com', '这是flask测试邮箱', '正文', 1, '2022-10-11 14:10:30');
INSERT INTO `admin_mail` VALUES (6, '1242733702@qq.com', '222', '333', 1, '2022-11-03 16:28:42');

-- ----------------------------
-- Table structure for admin_photo
-- ----------------------------
DROP TABLE IF EXISTS `admin_photo`;
CREATE TABLE `admin_photo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `mime` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `size` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_photo
-- ----------------------------
INSERT INTO `admin_photo` VALUES (3, '6958819_pear-admin_1607443454_1.png', 'http://127.0.0.1:5000/_uploads/photos/6958819_pear-admin_1607443454_1.png', 'image/png', '2204', '2021-03-19 18:53:02');
INSERT INTO `admin_photo` VALUES (17, '1617291580000.jpg', 'http://127.0.0.1:5000/_uploads/photos/1617291580000.jpg', 'image/png', '94211', '2021-04-01 23:39:41');
INSERT INTO `admin_photo` VALUES (50, 'Snipaste_2022-11-01_09-13-51.png', '/_uploads/photos/Snipaste_2022-11-01_09-13-51.png', 'image/png', '267545', '2022-11-04 11:50:05');
INSERT INTO `admin_photo` VALUES (51, '1667792319000.jpg', '/_uploads/photos/1667792319000.jpg', 'image/png', '97954', '2022-11-07 11:38:40');
INSERT INTO `admin_photo` VALUES (52, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532.jpg', 'image/jpeg', '251399', '2022-11-07 14:48:18');
INSERT INTO `admin_photo` VALUES (53, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_1.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_1.jpg', 'image/jpeg', '251399', '2022-11-07 14:53:30');
INSERT INTO `admin_photo` VALUES (54, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_2.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_2.jpg', 'image/jpeg', '251399', '2022-11-07 14:54:04');
INSERT INTO `admin_photo` VALUES (55, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_3.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_3.jpg', 'image/jpeg', '251399', '2022-11-07 14:58:17');
INSERT INTO `admin_photo` VALUES (56, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_4.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_4.jpg', 'image/jpeg', '251399', '2022-11-07 15:03:06');
INSERT INTO `admin_photo` VALUES (57, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_5.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_5.jpg', 'image/jpeg', '251399', '2022-11-07 17:13:09');
INSERT INTO `admin_photo` VALUES (58, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_6.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_6.jpg', 'image/jpeg', '251399', '2022-11-07 17:14:36');
INSERT INTO `admin_photo` VALUES (59, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_7.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_7.jpg', 'image/jpeg', '251399', '2022-11-07 17:15:30');
INSERT INTO `admin_photo` VALUES (60, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_8.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_8.jpg', 'image/jpeg', '251399', '2022-11-07 17:16:22');
INSERT INTO `admin_photo` VALUES (61, '38dbb6fd5266d01649c0d53d7ccae00c34fa3532_9.jpg', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_9.jpg', 'image/jpeg', '251399', '2022-11-07 17:16:48');
INSERT INTO `admin_photo` VALUES (62, '49.jpg', '/_uploads/photos/49.jpg', 'image/jpeg', '326005', '2022-11-07 17:21:48');
INSERT INTO `admin_photo` VALUES (63, '49_1.jpg', '/_uploads/photos/49_1.jpg', 'image/jpeg', '326005', '2022-11-07 17:23:15');

-- ----------------------------
-- Table structure for admin_power
-- ----------------------------
DROP TABLE IF EXISTS `admin_power`;
CREATE TABLE `admin_power`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '权限名称',
  `type` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '权限类型',
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '权限标识',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '权限路径',
  `open_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '打开方式',
  `parent_id` varchar(19) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '父类编号',
  `icon` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `enable` int(11) NULL DEFAULT NULL COMMENT '是否开启',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_power
-- ----------------------------
INSERT INTO `admin_power` VALUES (1, '系统管理', '0', '', NULL, NULL, '0', 'layui-icon layui-icon-set-fill', 1, NULL, NULL, 1);
INSERT INTO `admin_power` VALUES (3, '用户管理', '1', 'admin:user:main', '/admin/user/', '_iframe', '1', 'layui-icon layui-icon layui-icon layui-icon layui-icon-rate', 1, NULL, NULL, 1);
INSERT INTO `admin_power` VALUES (4, '权限管理', '1', 'admin:power:main', '/admin/power/', '_iframe', '1', NULL, 2, NULL, NULL, 1);
INSERT INTO `admin_power` VALUES (9, '角色管理', '1', 'admin:role:main', '/admin/role', '_iframe', '1', 'layui-icon layui-icon-username', 2, '2021-03-16 22:24:58', '2021-03-25 19:15:24', 1);
INSERT INTO `admin_power` VALUES (12, '系统监控', '1', 'admin:monitor:main', '/admin/monitor', '_iframe', '1', 'layui-icon layui-icon-vercode', 5, '2021-03-18 22:05:19', '2021-03-25 19:15:27', 1);
INSERT INTO `admin_power` VALUES (13, '日志管理', '1', 'admin:log:main', '/admin/log', '_iframe', '1', 'layui-icon layui-icon-read', 4, '2021-03-18 22:37:10', '2021-06-03 11:06:25', 1);
INSERT INTO `admin_power` VALUES (17, '文件管理', '0', '', '', '', '0', 'layui-icon layui-icon-camera', 2, '2021-03-19 18:56:23', '2021-03-25 19:15:08', 1);
INSERT INTO `admin_power` VALUES (18, '图片上传', '1', 'admin:file:main', '/admin/file', '_iframe', '17', 'layui-icon layui-icon-camera', 5, '2021-03-19 18:57:19', '2021-03-25 19:15:13', 1);
INSERT INTO `admin_power` VALUES (21, '权限增加', '2', 'admin:power:add', '', '', '4', 'layui-icon layui-icon-add-circle', 1, '2021-03-22 19:43:52', '2021-03-25 19:15:22', 1);
INSERT INTO `admin_power` VALUES (22, '用户增加', '2', 'admin:user:add', '', '', '3', 'layui-icon layui-icon-add-circle', 1, '2021-03-22 19:45:40', '2021-03-25 19:15:17', 1);
INSERT INTO `admin_power` VALUES (23, '用户编辑', '2', 'admin:user:edit', '', '', '3', 'layui-icon layui-icon-rate', 2, '2021-03-22 19:46:15', '2021-03-25 19:15:18', 1);
INSERT INTO `admin_power` VALUES (24, '用户删除', '2', 'admin:user:remove', '', '', '3', 'layui-icon None', 3, '2021-03-22 19:46:51', '2021-03-25 19:15:18', 1);
INSERT INTO `admin_power` VALUES (25, '权限编辑', '2', 'admin:power:edit', '', '', '4', 'layui-icon layui-icon-edit', 2, '2021-03-22 19:47:36', '2021-03-25 19:15:22', 1);
INSERT INTO `admin_power` VALUES (26, '用户删除', '2', 'admin:power:remove', '', '', '4', 'layui-icon layui-icon-delete', 3, '2021-03-22 19:48:17', '2021-03-25 19:15:23', 1);
INSERT INTO `admin_power` VALUES (27, '用户增加', '2', 'admin:role:add', '', '', '9', 'layui-icon layui-icon-add-circle', 1, '2021-03-22 19:49:09', '2021-03-25 19:15:24', 1);
INSERT INTO `admin_power` VALUES (28, '角色编辑', '2', 'admin:role:edit', '', '', '9', 'layui-icon layui-icon-edit', 2, '2021-03-22 19:49:41', '2021-03-25 19:15:25', 1);
INSERT INTO `admin_power` VALUES (29, '角色删除', '2', 'admin:role:remove', '', '', '9', 'layui-icon layui-icon-delete', 3, '2021-03-22 19:50:15', '2021-03-25 19:15:26', 1);
INSERT INTO `admin_power` VALUES (30, '角色授权', '2', 'admin:role:power', '', '', '9', 'layui-icon layui-icon-component', 4, '2021-03-22 19:50:54', '2021-03-25 19:15:26', 1);
INSERT INTO `admin_power` VALUES (31, '图片增加', '2', 'admin:file:add', '', '', '18', 'layui-icon layui-icon-add-circle', 1, '2021-03-22 19:58:05', '2021-03-25 19:15:28', 1);
INSERT INTO `admin_power` VALUES (32, '图片删除', '2', 'admin:file:delete', '', '', '18', 'layui-icon layui-icon-delete', 2, '2021-03-22 19:58:45', '2021-03-25 19:15:29', 1);
INSERT INTO `admin_power` VALUES (44, '数据字典', '1', 'admin:dict:main', '/admin/dict', '_iframe', '1', 'layui-icon layui-icon-console', 6, '2021-04-16 13:59:49', '2021-04-16 13:59:49', 1);
INSERT INTO `admin_power` VALUES (45, '字典增加', '2', 'admin:dict:add', '', '', '44', 'layui-icon ', 1, '2021-04-16 14:00:59', '2021-04-16 14:00:59', 1);
INSERT INTO `admin_power` VALUES (46, '字典修改', '2', 'admin:dict:edit', '', '', '44', 'layui-icon ', 2, '2021-04-16 14:01:33', '2021-04-16 14:01:33', 1);
INSERT INTO `admin_power` VALUES (47, '字典删除', '2', 'admin:dict:remove', '', '', '44', 'layui-icon ', 3, '2021-04-16 14:02:06', '2021-04-16 14:02:06', 1);
INSERT INTO `admin_power` VALUES (48, '部门管理', '1', 'admin:dept:main', '/dept', '_iframe', '1', 'layui-icon layui-icon-group', 3, '2021-06-01 16:22:11', '2021-07-07 13:49:39', 1);
INSERT INTO `admin_power` VALUES (49, '部门增加', '2', 'admin:dept:add', '', '', '48', 'layui-icon None', 1, '2021-06-01 17:35:52', '2021-06-01 17:36:15', 1);
INSERT INTO `admin_power` VALUES (50, '部门编辑', '2', 'admin:dept:edit', '', '', '48', 'layui-icon ', 2, '2021-06-01 17:36:41', '2021-06-01 17:36:41', 1);
INSERT INTO `admin_power` VALUES (51, '部门删除', '2', 'admin:dept:remove', '', '', '48', 'layui-icon None', 3, '2021-06-01 17:37:15', '2021-06-01 17:37:26', 1);
INSERT INTO `admin_power` VALUES (52, '定时任务', '0', '', '', '', '0', 'layui-icon layui-icon-log', 3, '2021-06-22 21:09:01', '2021-06-22 21:09:01', 1);
INSERT INTO `admin_power` VALUES (53, '任务管理', '1', 'admin:task:main', '/admin/task', '_iframe', '52', 'layui-icon ', 1, '2021-06-22 21:15:00', '2021-06-22 21:15:00', 1);
INSERT INTO `admin_power` VALUES (54, '任务增加', '2', 'admin:task:add', '', '', '53', 'layui-icon ', 1, '2021-06-22 22:20:54', '2021-06-22 22:20:54', 1);
INSERT INTO `admin_power` VALUES (55, '任务修改', '2', 'admin:task:edit', '', '', '53', 'layui-icon ', 2, '2021-06-22 22:21:34', '2021-06-22 22:21:34', 1);
INSERT INTO `admin_power` VALUES (56, '任务删除', '2', 'admin:task:remove', '', '', '53', 'layui-icon ', 3, '2021-06-22 22:22:18', '2021-06-22 22:22:18', 1);
INSERT INTO `admin_power` VALUES (57, '邮件管理', '1', 'admin:mail:main', '/admin/mail', '_iframe', '1', 'layui-icon layui-icon layui-icon-release', 7, '2022-10-11 11:21:05', '2022-10-11 11:21:22', 1);
INSERT INTO `admin_power` VALUES (58, '邮件发送', '2', 'admin:mail:add', '', '', '57', 'layui-icon layui-icon-ok-circle', 1, '2022-10-11 11:22:26', '2022-10-11 11:22:26', 1);
INSERT INTO `admin_power` VALUES (59, '邮件删除', '2', 'admin:mail:remove', '', '', '57', 'layui-icon layui-icon layui-icon-close', 2, '2022-10-11 11:23:06', '2022-10-11 11:23:18', 1);
INSERT INTO `admin_power` VALUES (60, '博客管理', '0', '', '', '', '0', 'layui-icon layui-icon-read', 3, '2022-11-03 14:56:17', '2022-11-03 15:00:20', 1);
INSERT INTO `admin_power` VALUES (61, '文章管理', '1', 'blog:article:main', '/blog/article', '_iframe', '60', 'layui-icon ', 1, '2022-11-03 14:57:02', '2022-11-03 14:57:02', 1);
INSERT INTO `admin_power` VALUES (62, '文章添加', '2', 'blog:article:add', '', '', '61', 'layui-icon ', 1, '2022-11-03 14:57:37', '2022-11-03 14:57:37', 1);
INSERT INTO `admin_power` VALUES (63, '文章修改', '2', 'blog:article:edit', '', '', '61', 'layui-icon ', 2, '2022-11-03 14:57:58', '2022-11-03 14:57:58', 1);
INSERT INTO `admin_power` VALUES (64, '文章删除', '2', 'blog:article:remove', '', '', '61', 'layui-icon layui-icon ', 3, '2022-11-03 14:58:19', '2022-11-03 14:58:29', 1);
INSERT INTO `admin_power` VALUES (65, '目录管理', '1', 'blog:catalog:main', '/blog/catalog', '_iframe', '60', 'layui-icon ', 2, '2022-11-03 15:01:02', '2022-11-03 15:01:02', 1);
INSERT INTO `admin_power` VALUES (66, '目录添加', '2', 'blog:catalog:add', '', '', '65', 'layui-icon ', 1, '2022-11-03 15:01:28', '2022-11-03 15:01:28', 1);
INSERT INTO `admin_power` VALUES (67, '目录修改', '2', 'blog:catalog:edit', '', '', '65', 'layui-icon ', 2, '2022-11-03 15:01:54', '2022-11-03 15:01:54', 1);
INSERT INTO `admin_power` VALUES (68, '目录删除', '2', 'blog:catalog:remove', '', '', '65', 'layui-icon ', 3, '2022-11-03 15:02:13', '2022-11-03 15:02:13', 1);
INSERT INTO `admin_power` VALUES (70, '标签管理', '1', 'blog:tag:main', '/blog/tag', '_iframe', '60', 'layui-icon ', 3, '2022-11-03 15:03:14', '2022-11-03 15:03:14', 1);
INSERT INTO `admin_power` VALUES (71, '标签添加', '2', 'blog:tag:add', '', '', '70', 'layui-icon ', 1, '2022-11-03 15:04:18', '2022-11-03 15:04:18', 1);
INSERT INTO `admin_power` VALUES (72, '标签修改', '2', 'blog:tag:edit', '', '', '70', 'layui-icon ', 2, '2022-11-03 15:04:36', '2022-11-03 15:04:36', 1);
INSERT INTO `admin_power` VALUES (73, '标签删除', '2', 'blog:tag:remove', '', '', '70', 'layui-icon ', 3, '2022-11-03 15:04:53', '2022-11-03 15:04:53', 1);
INSERT INTO `admin_power` VALUES (74, '网站管理', '1', 'admin:web:main', '/admin/web', '_iframe', '1', 'layui-icon ', 8, '2022-11-08 14:53:09', '2022-11-08 14:53:09', 1);
INSERT INTO `admin_power` VALUES (75, '网站添加', '2', 'admin:web:add', '', '', '74', 'layui-icon ', 1, '2022-11-08 14:54:22', '2022-11-08 14:54:22', 1);
INSERT INTO `admin_power` VALUES (76, '网站编辑', '2', 'admin:web:edit', '', '', '74', 'layui-icon ', 2, '2022-11-08 14:54:44', '2022-11-08 14:54:44', 1);
INSERT INTO `admin_power` VALUES (77, '网站删除', '2', 'admin:web:remove', '', '', '74', 'layui-icon ', 3, '2022-11-08 14:55:10', '2022-11-08 14:55:10', 1);

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '角色名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '角色标识',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `details` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '详情',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `enable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES (1, '管理员', 'admin', NULL, '管理员', 1, NULL, NULL, 1);
INSERT INTO `admin_role` VALUES (2, '普通用户', 'common', NULL, '只有查看，没有增删改权限', 2, '2021-03-22 20:02:38', '2021-04-01 22:29:56', 1);

-- ----------------------------
-- Table structure for admin_role_power
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_power`;
CREATE TABLE `admin_role_power`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识',
  `power_id` int(11) NULL DEFAULT NULL COMMENT '用户编号',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `power_id`(`power_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `admin_role_power_ibfk_1` FOREIGN KEY (`power_id`) REFERENCES `admin_power` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_role_power_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 387 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role_power
-- ----------------------------
INSERT INTO `admin_role_power` VALUES (265, 1, 2);
INSERT INTO `admin_role_power` VALUES (266, 3, 2);
INSERT INTO `admin_role_power` VALUES (267, 4, 2);
INSERT INTO `admin_role_power` VALUES (268, 9, 2);
INSERT INTO `admin_role_power` VALUES (269, 12, 2);
INSERT INTO `admin_role_power` VALUES (270, 13, 2);
INSERT INTO `admin_role_power` VALUES (271, 17, 2);
INSERT INTO `admin_role_power` VALUES (272, 18, 2);
INSERT INTO `admin_role_power` VALUES (273, 44, 2);
INSERT INTO `admin_role_power` VALUES (274, 48, 2);
INSERT INTO `admin_role_power` VALUES (334, 1, 1);
INSERT INTO `admin_role_power` VALUES (335, 3, 1);
INSERT INTO `admin_role_power` VALUES (336, 4, 1);
INSERT INTO `admin_role_power` VALUES (337, 9, 1);
INSERT INTO `admin_role_power` VALUES (338, 12, 1);
INSERT INTO `admin_role_power` VALUES (339, 13, 1);
INSERT INTO `admin_role_power` VALUES (340, 17, 1);
INSERT INTO `admin_role_power` VALUES (341, 18, 1);
INSERT INTO `admin_role_power` VALUES (342, 21, 1);
INSERT INTO `admin_role_power` VALUES (343, 22, 1);
INSERT INTO `admin_role_power` VALUES (344, 23, 1);
INSERT INTO `admin_role_power` VALUES (345, 24, 1);
INSERT INTO `admin_role_power` VALUES (346, 25, 1);
INSERT INTO `admin_role_power` VALUES (347, 26, 1);
INSERT INTO `admin_role_power` VALUES (348, 27, 1);
INSERT INTO `admin_role_power` VALUES (349, 28, 1);
INSERT INTO `admin_role_power` VALUES (350, 29, 1);
INSERT INTO `admin_role_power` VALUES (351, 30, 1);
INSERT INTO `admin_role_power` VALUES (352, 31, 1);
INSERT INTO `admin_role_power` VALUES (353, 32, 1);
INSERT INTO `admin_role_power` VALUES (354, 44, 1);
INSERT INTO `admin_role_power` VALUES (355, 45, 1);
INSERT INTO `admin_role_power` VALUES (356, 46, 1);
INSERT INTO `admin_role_power` VALUES (357, 47, 1);
INSERT INTO `admin_role_power` VALUES (358, 48, 1);
INSERT INTO `admin_role_power` VALUES (359, 49, 1);
INSERT INTO `admin_role_power` VALUES (360, 50, 1);
INSERT INTO `admin_role_power` VALUES (361, 51, 1);
INSERT INTO `admin_role_power` VALUES (362, 52, 1);
INSERT INTO `admin_role_power` VALUES (363, 53, 1);
INSERT INTO `admin_role_power` VALUES (364, 54, 1);
INSERT INTO `admin_role_power` VALUES (365, 55, 1);
INSERT INTO `admin_role_power` VALUES (366, 56, 1);
INSERT INTO `admin_role_power` VALUES (367, 57, 1);
INSERT INTO `admin_role_power` VALUES (368, 58, 1);
INSERT INTO `admin_role_power` VALUES (369, 59, 1);
INSERT INTO `admin_role_power` VALUES (370, 60, 1);
INSERT INTO `admin_role_power` VALUES (371, 61, 1);
INSERT INTO `admin_role_power` VALUES (372, 62, 1);
INSERT INTO `admin_role_power` VALUES (373, 63, 1);
INSERT INTO `admin_role_power` VALUES (374, 64, 1);
INSERT INTO `admin_role_power` VALUES (375, 65, 1);
INSERT INTO `admin_role_power` VALUES (376, 66, 1);
INSERT INTO `admin_role_power` VALUES (377, 67, 1);
INSERT INTO `admin_role_power` VALUES (378, 68, 1);
INSERT INTO `admin_role_power` VALUES (379, 70, 1);
INSERT INTO `admin_role_power` VALUES (380, 71, 1);
INSERT INTO `admin_role_power` VALUES (381, 72, 1);
INSERT INTO `admin_role_power` VALUES (382, 73, 1);
INSERT INTO `admin_role_power` VALUES (383, 74, 1);
INSERT INTO `admin_role_power` VALUES (384, 75, 1);
INSERT INTO `admin_role_power` VALUES (385, 76, 1);
INSERT INTO `admin_role_power` VALUES (386, 77, 1);

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `password_hash` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '哈希密码',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `enable` int(11) NULL DEFAULT NULL COMMENT '启用',
  `realname` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '真实名字',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES (1, 'admin', 'pbkdf2:sha256:150000$raM7mDSr$58fe069c3eac01531fc8af85e6fc200655dd2588090530084d182e6ec9d52c85', NULL, '2022-11-07 11:38:40', 1, '超级管理', '要是不能把握时机，就要终身蹭蹬，一事无成！', '/_uploads/photos/1667792319000.jpg', 1);
INSERT INTO `admin_user` VALUES (7, 'test', 'pbkdf2:sha256:150000$cRS8bYNh$adb57e64d929863cf159f924f74d0634f1fecc46dba749f1bfaca03da6d2e3ac', '2021-03-22 20:03:42', '2021-06-01 17:29:47', 1, '超级管理', '要是不能把握时机，就要终身蹭蹬，一事无成', '/static/admin/admin/images/avatar.jpg', 1);
INSERT INTO `admin_user` VALUES (8, 'wind', 'pbkdf2:sha256:150000$skME1obT$6a2c20cd29f89d7d2f21d9e373a7e3445f70ebce3ef1c3a555e42a7d17170b37', '2021-06-01 17:30:39', '2021-06-01 17:30:52', 1, '风', NULL, '/static/admin/admin/images/avatar.jpg', 7);

-- ----------------------------
-- Table structure for admin_user_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_role`;
CREATE TABLE `admin_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户编号',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `admin_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_user_role
-- ----------------------------
INSERT INTO `admin_user_role` VALUES (21, 1, 1);
INSERT INTO `admin_user_role` VALUES (22, 7, 2);
INSERT INTO `admin_user_role` VALUES (24, 8, 2);

-- ----------------------------
-- Table structure for admin_web
-- ----------------------------
DROP TABLE IF EXISTS `admin_web`;
CREATE TABLE `admin_web`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签编号ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '网站名称，可通过name查找',
  `info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '网站信息',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态(1发布,0不发布)',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_web
-- ----------------------------
INSERT INTO `admin_web` VALUES (1, '2233', 'null', NULL, '2022-11-08 16:05:34', '2022-11-08 16:05:34');
INSERT INTO `admin_web` VALUES (2, '333', '{\"33\": \"44\"}', NULL, '2022-11-08 16:12:15', '2022-11-08 16:49:26');
INSERT INTO `admin_web` VALUES (3, '222', '\"333\"', NULL, '2022-11-09 14:29:53', '2022-11-09 14:29:53');
INSERT INTO `admin_web` VALUES (4, '33', '44', NULL, '2022-11-09 14:40:37', '2022-11-09 14:40:37');
INSERT INTO `admin_web` VALUES (5, '44', '{\"array\":[1,2,3],\"boolean\":true,\"null\":123,\"number\":123,\"object\":{\"a\":\"b\",\"c\":\"d\"},\"string\":\"Hello World\"}', NULL, '2022-11-09 14:41:19', '2022-11-09 14:56:08');
INSERT INTO `admin_web` VALUES (6, '', '{\"array\":[1,2,3],\"boolean\":false,\"null\":null,\"number\":123,\"object\":{\"a\":\"b\",\"c\":\"d\"},\"string\":\"Hello World\"}', NULL, '2022-11-09 14:56:30', '2022-11-09 14:56:30');
INSERT INTO `admin_web` VALUES (7, 'blog', '{\"config\":{\"username\":\"mengfu\",\"avatarPath\":\"https://foruda.gitee.com/avatar/1663660348394169712/2035881_mengfu188_1663660348.png!avatar200\",\"description\":\"Your blog description\",\"name\":\"mengfu188\",\"blogTitle\":\"Bloger Haha\",\"blogStartYear\":2022},\"contacts\":[{\"name\":\"Contact method\",\"value\":\"Contact information, such as your email address\",\"link\":\"Contact link, such as `mailto:email@example.com`\"},{\"name\":\"Contact method\",\"value\":\"Contact information, such as your email address\",\"link\":\"Contact link, such as `mailto:email@example.com`\"}],\"about\":\"关于我介绍\",\"project_desc\":\"项目介绍\",\"projects\":[{\"name\":\"Project name\",\"des\":\"Project description\",\"img\":\"Project cover image url\",\"link\":\"Project homepage url\",\"github\":\"Project GitHub url\"}],\"links\":[{\"title\":\"Website title\",\"desc\":\"Website description\",\"link\":\"Website link\"},{\"title\":\"Website title\",\"desc\":\"Website description\",\"link\":\"Website link\"},{\"title\":\"Website title\",\"desc\":\"Website description\",\"link\":\"Website link\"}]}', 1, '2022-11-09 15:44:08', '2022-11-09 16:39:35');

-- ----------------------------
-- Table structure for alembic_version
-- ----------------------------
DROP TABLE IF EXISTS `alembic_version`;
CREATE TABLE `alembic_version`  (
  `version_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version_num`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alembic_version
-- ----------------------------
INSERT INTO `alembic_version` VALUES ('0d344926a0c1');

-- ----------------------------
-- Table structure for blog_article
-- ----------------------------
DROP TABLE IF EXISTS `blog_article`;
CREATE TABLE `blog_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章编号ID',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '作者id',
  `catalog_id` int(11) NULL DEFAULT NULL COMMENT '所属目录',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态(1发布,0不发布)',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blog_article
-- ----------------------------
INSERT INTO `blog_article` VALUES (36, 1, 0, '456', '<p><img src=\"/_uploads/photos/Snipaste_2022-11-01_09-13-51.png\" /></p>\n<p>内容1</p>\n<p>内容2</p>', 1, '2022-11-04 11:50:13', '2022-11-07 17:23:16', '/_uploads/photos/49_1.jpg');
INSERT INTO `blog_article` VALUES (37, 1, 0, '3425', '<p>345345<img src=\"/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_8.jpg\" alt=\"\" width=\"800\" height=\"800\" /></p>', 1, '2022-11-07 15:00:26', '2022-11-07 17:16:25', 'https://dev-to-uploads.s3.amazonaws.com/i/95lvt23xz4ozer5byomi.png');
INSERT INTO `blog_article` VALUES (38, 1, 0, '4234', '<p>3453455555</p>', 1, '2022-11-07 15:02:45', '2022-11-07 15:02:45', '');
INSERT INTO `blog_article` VALUES (39, 1, 0, '666666', '<p>5555555</p>', 1, '2022-11-07 15:03:07', '2022-11-07 15:04:33', '/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_4.jpg');
INSERT INTO `blog_article` VALUES (40, 1, 0, '', '<p><img src=\"/_uploads/photos/38dbb6fd5266d01649c0d53d7ccae00c34fa3532_9.jpg\" /></p>', 1, '2022-11-07 17:16:49', '2022-11-07 17:16:49', '');

-- ----------------------------
-- Table structure for blog_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `blog_article_tag`;
CREATE TABLE `blog_article_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识',
  `article_id` int(11) NULL DEFAULT NULL COMMENT '文章编号',
  `tag_id` int(11) NULL DEFAULT NULL COMMENT '标签编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `article_id`(`article_id`) USING BTREE,
  INDEX `tag_id`(`tag_id`) USING BTREE,
  CONSTRAINT `blog_article_tag_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `blog_article` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `blog_article_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `blog_tag` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blog_article_tag
-- ----------------------------
INSERT INTO `blog_article_tag` VALUES (2, 39, 1);
INSERT INTO `blog_article_tag` VALUES (3, 39, 4);
INSERT INTO `blog_article_tag` VALUES (4, 36, 4);

-- ----------------------------
-- Table structure for blog_catalog
-- ----------------------------
DROP TABLE IF EXISTS `blog_catalog`;
CREATE TABLE `blog_catalog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '目录编号ID',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父目录',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态(1发布,0不发布)',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blog_catalog
-- ----------------------------
INSERT INTO `blog_catalog` VALUES (1, 0, 1, '1', 1, 1, '2022-10-05 16:17:30', '2022-10-05 16:17:32');
INSERT INTO `blog_catalog` VALUES (2, 1, 1, '2', 2, 1, '2022-10-05 16:34:25', '2022-10-05 16:34:28');
INSERT INTO `blog_catalog` VALUES (3, 0, NULL, '22', 2, 1, '2022-10-06 15:30:24', '2022-10-06 15:30:24');
INSERT INTO `blog_catalog` VALUES (6, 3, 1, '333333', 2, 1, '2022-10-07 16:59:04', '2022-10-07 16:59:04');
INSERT INTO `blog_catalog` VALUES (7, 0, 1, '4445', 555, 1, '2022-10-08 11:15:06', '2022-10-08 11:15:15');

-- ----------------------------
-- Table structure for blog_tag
-- ----------------------------
DROP TABLE IF EXISTS `blog_tag`;
CREATE TABLE `blog_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签编号ID',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '所属用户id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签名字',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态(1发布,0不发布)',
  `create_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blog_tag
-- ----------------------------
INSERT INTO `blog_tag` VALUES (1, 1, 'tag1', 1, '2022-10-08 14:50:25', '2022-10-08 14:55:01');
INSERT INTO `blog_tag` VALUES (4, 1, 'tag4', 1, '2022-10-08 14:56:53', '2022-10-08 14:56:53');

SET FOREIGN_KEY_CHECKS = 1;
