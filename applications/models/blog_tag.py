import datetime
from applications.extensions import db


class Tag(db.Model):
    __tablename__ = 'blog_tag'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, comment="标签编号ID")
    user_id = db.Column(db.Integer, comment="所属用户id")
    name = db.Column(db.String(50), comment="标签名字")
    status = db.Column(db.Integer, comment='状态(1发布,0不发布)')
    create_at = db.Column(db.DateTime, default=datetime.datetime.now, comment='创建时间')
    update_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')
    # articles = db.relationship('Article', secondary="blog_article_tag", backref=db.backref('tag'))