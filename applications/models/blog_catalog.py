import datetime
from applications.extensions import db


class Catalog(db.Model):
    __tablename__ = 'blog_catalog'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, comment="目录编号ID")
    parent_id = db.Column(db.Integer, comment="父目录")
    user_id = db.Column(db.Integer, comment="用户id")
    name = db.Column(db.String(50), comment="名称")
    sort = db.Column(db.Integer, comment="排序")
    status = db.Column(db.Integer, comment='状态(1发布,0不发布)')
    create_at = db.Column(db.DateTime, default=datetime.datetime.now, comment='创建时间')
    update_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')
