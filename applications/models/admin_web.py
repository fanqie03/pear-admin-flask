import datetime
from applications.extensions import db


class Web(db.Model):
    __tablename__ = 'admin_web'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, comment="标签编号ID")
    name = db.Column(db.String(50), comment="网站名称，可通过name查找")
    info = db.Column(db.String(), comment="网站信息")
    status = db.Column(db.Integer, comment='状态(1发布,0不发布)')
    create_at = db.Column(db.DateTime, default=datetime.datetime.now, comment='创建时间')
    update_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')