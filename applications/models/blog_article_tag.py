from applications.extensions import db

# 创建中间表
article_tag = db.Table(
    "blog_article_tag",  # 中间表名称
    db.Column("id", db.Integer, primary_key=True, autoincrement=True, comment='标识'),  # 主键
    db.Column("article_id", db.Integer, db.ForeignKey("blog_article.id"), comment='文章编号'),  # 属性 外键
    db.Column("tag_id", db.Integer, db.ForeignKey("blog_tag.id"), comment='标签编号'),  # 属性 外键
)
