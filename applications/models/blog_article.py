import datetime
from applications.extensions import db


class Article(db.Model):
    __tablename__ = 'blog_article'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, comment="文章编号ID")
    user_id = db.Column(db.Integer, comment="作者id")
    catalog_id = db.Column(db.Integer, comment="所属目录")
    title = db.Column(db.String(50), comment="标题")
    cover = db.Column(db.String(255), comment='封面', default="https://dev-to-uploads.s3.amazonaws.com/i/95lvt23xz4ozer5byomi.png")
    content = db.Column(db.Text, comment="内容")
    status = db.Column(db.Integer, comment='状态(1发布,0不发布)')
    create_at = db.Column(db.DateTime, default=datetime.datetime.now, comment='创建时间')
    update_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')
    tag = db.relationship('Tag', secondary="blog_article_tag", backref=db.backref('article'))
