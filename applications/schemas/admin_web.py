from applications.extensions import ma
from marshmallow import fields, validate


class WebOutSchema(ma.Schema):
    id = fields.Integer(required=True)
    name = fields.Str(attribute='name')
    info = fields.Str()
    status = fields.Str(validate=validate.OneOf(["0", "1"]))
    create_at = fields.DateTime()
    update_at = fields.DateTime()
