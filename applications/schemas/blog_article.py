import re

from applications.extensions import ma
from applications.models import User, Catalog, Tag
from applications.common.utils.validate import get_text_by_html
from marshmallow import fields, validate
from flask import request



class ArticleOutSchema(ma.Schema):
    id = fields.Integer(required=True)
    realname = fields.Method("get_realname")
    catalog_name = fields.Method("get_catalog_name")
    title = fields.Str(required=True)
    cover = fields.Str()
    content = fields.Str(validate=validate.Email())
    status = fields.Str(validate=validate.OneOf(["0", "1"]))

    def get_realname(self, obj):
        if obj.user_id != None:
            return User.query.filter_by(id=obj.user_id).first().realname
        else:
            return None

    def get_catalog_name(self, obj):
        if obj.user_id != None:
            item = Catalog.query.filter_by(id=obj.catalog_id).first()
            if item:
                return item.name
        return None


class ArticleOutSchema2(ma.Schema):
    """博客前端"""
    id = fields.Integer(required=True)
    title = fields.Str(required=True)
    content = fields.Method("get_content")
    tags = fields.Method("get_tags")
    cover = fields.Method("get_cover")
    des = fields.Method("get_des")
    date = fields.Method("get_date")

    def get_content(self, obj):
        # 替换里面的图片链接
        return re.sub('src="/_uploads', f'src="{request.host_url}/_uploads', obj.content)

    def get_cover(self, obj):
        url = obj.cover
        if url is None or url.startswith('http'):
            return url
        if request.host_url and url:  # 前后端分离，图片链接要加上domain
            url = request.host_url + url
        return url

    def get_tags(self, obj):
        return [x.name for x in obj.tag]

    def get_des(self, obj):
        return get_text_by_html(obj.content)[:50]

    def get_date(self, obj):
        return [obj.create_at.year, obj.create_at.month, obj.create_at.day]

    def get_realname(self, obj):
        if obj.user_id != None:
            return User.query.filter_by(id=obj.user_id).first().realname
        else:
            return None

    def get_catalog_name(self, obj):
        if obj.user_id != None:
            item = Catalog.query.filter_by(id=obj.catalog_id).first()
            if item:
                return item.name
        return None
