from applications.extensions import ma
from applications.models import User
from applications.schemas.blog_article import ArticleOutSchema2
from marshmallow import fields, validate


class TagOutSchema(ma.Schema):
    id = fields.Integer(required=True)
    realname = fields.Method("get_realname")
    name = fields.Str(attribute='name')
    status = fields.Str(validate=validate.OneOf(["0", "1"]))

    def get_realname(self, obj):
        if obj.user_id != None:
            return User.query.filter_by(id=obj.user_id).first().realname
        else:
            return None


class TagOutSchema2(ma.Schema):
    id = fields.Integer(required=True)
    name = fields.Str(attribute='name')
    article_length = fields.Method("get_article_length")
    article = fields.List(fields.Nested(ArticleOutSchema2))

    def get_article_length(self, obj):
        return len(obj.article)

