from applications.extensions import ma
from applications.models import User
from marshmallow import fields, validate


class CatalogOutSchema(ma.Schema):
    id = fields.Integer(required=True, attribute='id')
    parent_id = fields.Integer(required=True, attribute='parent_id')
    realname = fields.Method("get_realname")
    name = fields.Str()
    sort = fields.Integer()
    status = fields.Str(validate=validate.OneOf(["0", "1"]))

    def get_realname(self, obj):
        if obj.user_id != None:
            return User.query.filter_by(id=obj.user_id).first().realname
        else:
            return None

