# xss过滤
from flask import abort, make_response, jsonify
import bs4
from .pxfilter import XssHtml


def xss_escape(s: str):
    if s is None:
        return None
    # else:
    #     return s.replace("&", "&amp;").replace(">", "&gt;").replace("<", "&lt;").replace("'", "&#39;").replace('"',
    #                                                                                                            "&#34;")
    else:
        parser = XssHtml()
        parser.feed(s)
        parser.close()
        return parser.getHtml()


def get_text_by_html(content, join_text='\n'):
    soup = bs4.BeautifulSoup(content, 'html.parser')
    res = []
    for data in soup.find_all("p"):
        res.append(data.get_text())
    return join_text.join(res)


def check_data(schema, data):
    errors = schema.validate(data)
    for k, v in errors.items():
        for i in v:
            # print("{}{}".format(k, i))
            msg = "{}{}".format(k, i)
    if errors:
        abort(make_response(jsonify(result=False, msg=msg), 200))
