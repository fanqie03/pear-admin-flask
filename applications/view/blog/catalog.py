from flask import Blueprint, render_template, request, jsonify
from flask_login import current_user

from applications.common import curd
from applications.common.admin import is_admin
from applications.common.helper import ModelFilter
from applications.common.utils import validate
from applications.common.utils.http import success_api, fail_api, table_api
from applications.common.utils.rights import authorize
from applications.common.utils.validate import xss_escape
from applications.extensions import db
from applications.models import Dept, User, Catalog, Article
from applications.schemas import DeptOutSchema, CatalogOutSchema

catalog_bp = Blueprint('catalog', __name__, url_prefix='/catalog')


@catalog_bp.get('/')
@authorize("blog:catalog:main", log=True)
def main():
    return render_template('blog/catalog/main.html')


@catalog_bp.post('/data')
@authorize("blog:catalog:main", log=True)
def data():

    # 获取请求参数
    name = xss_escape(request.args.get('name', type=str))
    # 查询参数构造
    mf = ModelFilter()
    if name:
        mf.contains(field_name="name", value=name)
    # 如果不是超级管理员就只可以获得当前用户的文章
    if not is_admin():
        mf.exact(field_name="user_id", value=current_user.id)

    # orm查询
    catalog = Catalog.query.filter(mf.get_filter(model=Catalog)).order_by(Catalog.sort).all()
    catalog_data = curd.model_to_dicts(schema=CatalogOutSchema, data=catalog)
    res = {
        'data': catalog_data
    }
    return jsonify(res)


@catalog_bp.get('/add')
@authorize("blog:catalog:add", log=True)
def add():
    return render_template('blog/catalog/add.html')


@catalog_bp.get('/tree')
@authorize("blog:catalog:main", log=True)
def tree():
    catalog = Catalog.query.order_by(Catalog.sort).all()
    catalog_data = curd.model_to_dicts(schema=CatalogOutSchema, data=catalog)
    catalog_data.append({"id": 0, "name": "顶级目录", "parent_id": -1})
    res = {
        "status": {"code": 200, "message": "默认"},
        "data": catalog_data
    }
    return jsonify(res)


@catalog_bp.post('/save')
@authorize("blog:catalog:add", log=True)
def save():
    req_json = request.json
    catalog = Catalog(
        user_id=current_user.id,
        parent_id=req_json.get('parent_id'),
        name=xss_escape(req_json.get('name')),
        sort=xss_escape(req_json.get('sort')),
        status=xss_escape(req_json.get('status')),
    )
    r = db.session.add(catalog)
    db.session.commit()
    return success_api(msg="成功")


@catalog_bp.get('/edit/<int:_id>')
@authorize("blog:catalog:edit", log=True)
def edit(_id):
    catalog = curd.get_one_by_id(model=Catalog, id=_id)
    return render_template('blog/catalog/edit.html', catalog=catalog)


# 启用
@catalog_bp.put('/enable')
@authorize("blog:catalog:edit", log=True)
def enable():
    id = request.json.get('id')
    if id:
        enable = 1
        d = Catalog.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="启用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


# 禁用
@catalog_bp.put('/disable')
@authorize("blog:catalog:edit", log=True)
def dis_enable():
    id = request.json.get('id')
    if id:
        enable = 0
        d = Catalog.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="禁用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


@catalog_bp.put('/update')
@authorize("blog:catalog:edit", log=True)
def update():
    json = request.json
    id = json.get("id"),
    data = {
        "parent_id": validate.xss_escape(json.get("parent_id")),
        "name": validate.xss_escape(json.get("name")),
        "sort": validate.xss_escape(json.get("sort")),
        "status": validate.xss_escape(json.get("status")),
    }
    d = Catalog.query.filter_by(id=id).update(data)
    if not d:
        return fail_api(msg="更新失败")
    db.session.commit()
    return success_api(msg="更新成功")


@catalog_bp.delete('/remove/<int:_id>')
@authorize("blog:catalog:remove", log=True)
def remove(_id):
    d = Catalog.query.filter_by(id=_id).delete()
    if not d:
        return fail_api(msg="删除失败")
    # TODO 查找所有文章，将目录id置为空，没找到？还要考虑子目录
    # res = Article.query.filter_by(catalog_id=_id).update({"catalog_id": None})
    # if not res:
    #     return fail_api(msg="相关文章的目录属性删除失败")
    db.session.commit()
    return success_api(msg="更新成功")


@catalog_bp.delete('/batchRemove')
@authorize("blog:catalog:remove", log=True)
def batch_remove():
    ids = request.form.getlist('ids[]')
    for id in ids:
        res = Catalog.query.filter_by(id=id).delete()
        if not res:
            return fail_api(msg="删除失败")
    
        # TODO 查找所有文章，将目录id置为空，没找到？
    db.session.commit()
    return success_api(msg="批量删除成功")
