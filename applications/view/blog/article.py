from flask import Blueprint, render_template, request, jsonify
from flask_login import current_user

from applications.common import curd
from applications.common.admin import is_admin
from applications.common.helper import ModelFilter
from applications.common.utils import validate
from applications.common.utils.http import success_api, fail_api, table_api
from applications.common.utils.rights import authorize
from applications.common.utils.validate import xss_escape
from applications.extensions import db
from applications.models import Dept, User, Article, Tag
from applications.schemas import DeptOutSchema, ArticleOutSchema, ArticleOutSchema2
from applications.common.utils import upload as upload_curd

article_bp = Blueprint('article', __name__, url_prefix='/article')


@article_bp.get('/')
@authorize("blog:article:main", log=True)
def main():
    return render_template('blog/article/main.html')


@article_bp.get('/data')
@authorize("blog:article:main", log=True)
def data():
    # 获取请求参数
    title = xss_escape(request.args.get('title', type=str))
    content = xss_escape(request.args.get('content', type=str))
    # 查询参数构造
    mf = ModelFilter()
    if title:
        mf.contains(field_name="title", value=title)
    if content:
        mf.contains(field_name="content", value=content)
    # 如果不是超级管理员就只可以获得当前用户的文章
    if not is_admin():
        mf.exact(field_name="user_id", value=current_user.id)

    # orm查询
    # 使用分页获取data需要.items
    article = Article.query.filter(mf.get_filter(model=Article)).layui_paginate()
    count = article.total
    return table_api(data=curd.model_to_dicts(schema=ArticleOutSchema, data=article.items), count=count)


@article_bp.get('/add')
@authorize("blog:article:add", log=True)
def add():
    tags = Tag.query.filter_by(user_id=current_user.id).all()
    return render_template('blog/article/add.html', tags=tags)


#   上传接口
@article_bp.post('/upload')
@authorize("blog:article:add", log=True)
def upload_api():
    if 'edit' in request.files:  # layui tinymce 上传默认字段为edit
        photo = request.files['edit']
        mime = request.files['edit'].content_type

        file_url = upload_curd.upload_one(photo=photo, mime=mime)
        res = {
            "msg": "上传成功",
            "code": 0,
            "success": True,
            "data": file_url  # 回传data即可
        }
        return jsonify(res)
    return fail_api()


@article_bp.post('/save')
@authorize("blog:article:add", log=True)
def save():
    req_json = request.json
    a = req_json.get("tag_ids")
    article = Article(
        user_id=current_user.id,
        title=req_json.get('title'),
        content=xss_escape(req_json.get('content')),
        catalog_id=xss_escape(req_json.get('parent_id')),
        status=xss_escape(req_json.get('status')),
        cover=xss_escape(req_json.get('cover')),
    )
    tag_ids = a.split(',')
    tags = Tag.query.filter(Tag.id.in_(tag_ids)).all()
    for t in tags:
        article.tag.append(t)
    r = db.session.add(article)
    db.session.commit()
    return success_api(msg="成功")


@article_bp.get('/edit/<int:_id>')
@authorize("blog:article:edit", log=True)
def edit(_id):
    article = curd.get_one_by_id(model=Article, id=_id)
    tags = Tag.query.all()
    checked_tags = []
    for t in article.tag:
        checked_tags.append(t.id)
    return render_template('blog/article/edit.html', article=article, tags=tags, checked_tags=checked_tags)


# 启用
@article_bp.put('/enable')
@authorize("blog:article:edit", log=True)
def enable():
    id = request.json.get('id')
    if id:
        enable = 1
        d = Article.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="启用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


# 禁用
@article_bp.put('/disable')
@authorize("blog:article:edit", log=True)
def dis_enable():
    id = request.json.get('id')
    if id:
        enable = 0
        d = Article.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="禁用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


@article_bp.put('/update')
@authorize("blog:article:edit", log=True)
def update():
    json = request.json
    id = json.get("id"),
    a = xss_escape(json.get("tag_ids"))
    tag_ids = a.split(',')
    article = dict(
        title=json.get('title'),
        content=xss_escape(json.get('content')),
        catalog_id=xss_escape(json.get('parent_id')),
        cover=xss_escape(json.get('cover')),
    )
    d = Article.query.filter_by(id=id).update(article)
    if not d:
        return fail_api(msg="更新失败")

    article = Article.query.filter_by(id=id).first()
    tags = Tag.query.filter(Tag.id.in_(tag_ids)).all()
    article.tag = tags

    db.session.commit()
    return success_api(msg="更新成功")


@article_bp.delete('/remove/<int:_id>')
@authorize("blog:article:remove", log=True)
def remove(_id):
    article = Article.query.filter_by(id=_id).first()
    article.tag = []

    d = Article.query.filter_by(id=_id).delete()
    db.session.commit()
    if not d:
        return fail_api(msg="删除失败")
    else:
        return success_api(msg="删除成功")


@article_bp.delete('/batchRemove')
@authorize("blog:article:remove", log=True)
def batch_remove():
    ids = request.form.getlist('ids[]')
    for id in ids:
        article = Article.query.filter_by(id=id).first()
        article.tag = []

        res = Article.query.filter_by(id=id).delete()
        if not res:
            return fail_api(msg="删除失败")
    db.session.commit()
    return success_api(msg="批量删除成功")


"""
前台访问
"""


@article_bp.get('/list')
def list_data():
    # 获取请求参数
    _id = xss_escape(request.args.get('id', type=str))
    # 查询参数构造
    mf = ModelFilter()
    if _id:
        mf.contains(field_name="id", value=_id)


    # orm查询
    # 使用分页获取data需要.items
    articles = Article.query.filter(mf.get_filter(model=Article)).layui_paginate()
    count = articles.total
    return table_api(data=curd.model_to_dicts(schema=ArticleOutSchema2, data=articles.items),
                     count=count, limit=request.args.get('limit', type=int))

