#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/5 11:47
# file: index.py
# author: mengfu188
from flask.blueprints import Blueprint
from flask import send_from_directory

index_bp = Blueprint('index', __name__, url_prefix='/index')

@index_bp.get("/<path:filename>")
def index(filename):
    return send_from_directory('static/blog', filename)




