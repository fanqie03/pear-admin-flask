#!/usr/bin/env python
# -*- coding: utf-8 -*-
# time: 2022/10/5 11:47
# file: __init__.py
# author: mengfu188
from flask.blueprints import Blueprint

from .article import article_bp
from .catalog import catalog_bp
from .tag import tag_bp
from .index import index_bp

blog_bp = Blueprint('blog', __name__, url_prefix='/blog')


def register_blog_views(app):
    blog_bp.register_blueprint(article_bp)
    blog_bp.register_blueprint(catalog_bp)
    blog_bp.register_blueprint(tag_bp)
    blog_bp.register_blueprint(index_bp)
    app.register_blueprint(blog_bp)


