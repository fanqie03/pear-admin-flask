from flask import Blueprint, render_template, request, jsonify
from flask_login import current_user

from applications.common import curd
from applications.common.admin import is_admin
from applications.common.helper import ModelFilter
from applications.common.utils import validate
from applications.common.utils.http import success_api, fail_api, table_api
from applications.common.utils.rights import authorize
from applications.common.utils.validate import xss_escape
from applications.extensions import db
from applications.models import Dept, User, Article, Tag
from applications.schemas import DeptOutSchema, ArticleOutSchema, TagOutSchema, TagOutSchema2

tag_bp = Blueprint('tag', __name__, url_prefix='/tag')


@tag_bp.get('/')
@authorize("blog:tag:main", log=True)
def main():
    return render_template('blog/tag/main.html')


@tag_bp.get('/data')
@authorize("blog:tag:main", log=True)
def data():
    # 获取请求参数
    name = xss_escape(request.args.get('name', type=str))
    # 查询参数构造
    mf = ModelFilter()
    if name:
        mf.contains(field_name="name", value=name)
    # 如果不是超级管理员就只可以获得当前用户的文章
    if not is_admin():
        mf.exact(field_name="user_id", value=current_user.id)

    # orm查询
    # 使用分页获取data需要.items
    tag = Tag.query.filter(mf.get_filter(model=Tag)).layui_paginate()
    count = tag.total
    return table_api(data=curd.model_to_dicts(schema=TagOutSchema, data=tag.items), count=count)


@tag_bp.get('/add')
@authorize("blog:tag:add", log=True)
def add():
    return render_template('blog/tag/add.html')


@tag_bp.post('/save')
@authorize("blog:tag:add", log=True)
def save():
    req_json = request.json
    tag = Tag(
        user_id=current_user.id,
        name=req_json.get('name'),
        status=xss_escape(req_json.get('status')),
    )
    r = db.session.add(tag)
    db.session.commit()
    return success_api(msg="成功")


@tag_bp.get('/edit/<int:_id>')
@authorize("blog:tag:edit", log=True)
def edit(_id):
    tag = curd.get_one_by_id(model=Tag, id=_id)
    return render_template('blog/tag/edit.html', tag=tag)


# 启用
@tag_bp.put('/enable')
@authorize("blog:tag:edit", log=True)
def enable():
    id = request.json.get('id')
    if id:
        enable = 1
        d = Tag.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="启用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


# 禁用
@tag_bp.put('/disable')
@authorize("blog:tag:edit", log=True)
def dis_enable():
    id = request.json.get('id')
    if id:
        enable = 0
        d = Tag.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="禁用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


@tag_bp.put('/update')
@authorize("blog:tag:edit", log=True)
def update():
    json = request.json
    id = json.get("id"),
    tag = dict(
        name=json.get('name'),
    )
    d = Tag.query.filter_by(id=id).update(tag)
    if not d:
        return fail_api(msg="更新失败")
    db.session.commit()
    return success_api(msg="更新成功")


@tag_bp.delete('/remove/<int:_id>')
@authorize("blog:tag:remove", log=True)
def remove(_id):
    d = Tag.query.filter_by(id=_id).delete()
    db.session.commit()
    if not d:
        return fail_api(msg="删除失败")
    else:
        return success_api(msg="删除成功")


@tag_bp.delete('/batchRemove')
@authorize("blog:tag:remove", log=True)
def batch_remove():
    ids = request.form.getlist('ids[]')
    for id in ids:
        res = Tag.query.filter_by(id=id).delete()
        if not res:
            return fail_api(msg="删除失败")
    db.session.commit()
    return success_api(msg="批量删除成功")

"""
前台访问
"""


@tag_bp.get('/list')
def list_data():
    # 获取请求参数
    _id = xss_escape(request.args.get('id', type=str))
    # 查询参数构造
    mf = ModelFilter()
    if _id:
        mf.contains(field_name="id", value=_id)


    # orm查询
    # 使用分页获取data需要.items
    tags = Tag.query.filter(mf.get_filter(model=Tag)).layui_paginate()
    count = tags.total
    return table_api(data=curd.model_to_dicts(schema=TagOutSchema2, data=tags.items),
                     count=count, limit=request.args.get('limit', type=int))

