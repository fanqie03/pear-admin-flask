import json

from flask import Blueprint, render_template, request, current_app, Markup
from flask_login import current_user
from flask_mail import Message

from applications.common import curd
from applications.common.curd import model_to_dicts
from applications.common.helper import ModelFilter
from applications.common.utils.http import table_api, fail_api, success_api
from applications.common.utils.rights import authorize
from applications.common.utils.validate import xss_escape
from applications.extensions import db, flask_mail
from applications.models import Web
from applications.schemas import WebOutSchema

admin_web = Blueprint('adminWeb', __name__, url_prefix='/admin/web')


# 用户管理
@admin_web.get('/')
@authorize("admin:web:main", log=True)
def main():
    return render_template('admin/web/main.html')


#   用户分页查询
@admin_web.get('/data')
@authorize("admin:web:main", log=True)
def data():
    # 获取请求参数
    name = xss_escape(request.args.get("name"))
    # 查询参数构造
    mf = ModelFilter()
    if name:
        mf.contains(field_name="name", value=name)
    # orm查询
    # 使用分页获取data需要.items
    webs = Web.query.filter(mf.get_filter(Web)).layui_paginate()
    count = webs.total
    # 返回api
    return table_api(data=model_to_dicts(schema=WebOutSchema, data=webs.items), count=count)


# 增加
@admin_web.get('/add')
@authorize("admin:web:add", log=True)
def add():
    return render_template('admin/web/add.html')


@admin_web.post('/save')
@authorize("admin:web:add", log=True)
def save():
    req_json = request.json
    name = xss_escape(req_json.get("name"))
    info = req_json.get('info')

    web = Web(name=name, info=info)

    db.session.add(web)
    db.session.commit()
    return success_api(msg="增加成功")



@admin_web.get('/edit/<int:_id>')
@authorize("admin:web:edit", log=True)
def edit(_id):
    web = curd.get_one_by_id(model=Web, id=_id)
    # flask为了安全，会把传给template的字段检查下
    web.info = Markup(web.info)
    return render_template('admin/web/edit.html', web=web)


@admin_web.put('/update')
@authorize("admin:web:edit", log=True)
def update():
    req_json = request.json
    id = req_json.get("id"),
    web = dict(
        name=req_json.get('name'),
        info=req_json.get('info')
    )
    d = Web.query.filter_by(id=id).update(web)
    if not d:
        return fail_api(msg="更新失败")

    db.session.commit()
    return success_api(msg="更新成功")

# 删除
@admin_web.delete('/remove/<int:id>')
@authorize("admin:web:remove", log=True)
def delete(id):
    res = Web.query.filter_by(id=id).delete()
    if not res:
        return fail_api(msg="删除失败")
    db.session.commit()
    return success_api(msg="删除成功")


# 启用
@admin_web.put('/enable')
@authorize("admin:web:edit", log=True)
def enable():
    id = request.json.get('id')
    if id:
        enable = 1
        d = Web.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="启用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


# 禁用
@admin_web.put('/disable')
@authorize("admin:web:edit", log=True)
def dis_enable():
    id = request.json.get('id')
    if id:
        enable = 0
        d = Web.query.filter_by(id=id).update({"status": enable})
        if d:
            db.session.commit()
            return success_api(msg="禁用成功")
        return fail_api(msg="出错啦")
    return fail_api(msg="数据错误")


# 批量删除
@admin_web.delete('/batchRemove')
@authorize("admin:web:remove", log=True)
def batch_remove():
    ids = request.form.getlist('ids[]')
    for id in ids:
        res = Web.query.filter_by(id=id).delete()
        if not res:
            return fail_api(msg="批量删除失败")
    db.session.commit()
    return success_api(msg="批量删除成功")

"""
前台访问
"""


@admin_web.get('/list')
def list_data():
    # 获取请求参数
    _id = xss_escape(request.args.get('id', type=str))
    name = xss_escape(request.args.get('name', type=str))
    # 查询参数构造
    mf = ModelFilter()
    if _id:
        mf.contains(field_name="id", value=_id)
    if name:
        mf.contains(field_name="name", value=name)


    # orm查询
    # 使用分页获取data需要.items
    webs = Web.query.filter(mf.get_filter(model=Web)).layui_paginate()
    count = webs.total
    return table_api(data=curd.model_to_dicts(schema=WebOutSchema, data=webs.items),
                     count=count, limit=request.args.get('limit', type=int))

