# -*- coding: utf-8 -*-
# @Time : 2022/11/4 17:43
# @Author : mengfu188
# @File : init_cors.py
# @Desc :

from flask_cors import CORS


def init_cors(app):
    cors = CORS(app)
    return cors
