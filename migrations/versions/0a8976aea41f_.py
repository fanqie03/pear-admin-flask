"""empty message

Revision ID: 0a8976aea41f
Revises: 7634e028e338
Create Date: 2022-11-03 14:17:04.060661

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0a8976aea41f'
down_revision = '7634e028e338'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('admin_mail',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='邮件编号'),
    sa.Column('receiver', sa.String(length=1024), nullable=True, comment='收件人邮箱'),
    sa.Column('subject', sa.String(length=128), nullable=True, comment='邮件主题'),
    sa.Column('content', sa.Text(), nullable=True, comment='邮件正文'),
    sa.Column('user_id', sa.Integer(), nullable=True, comment='发送人id'),
    sa.Column('create_at', sa.DateTime(), nullable=True, comment='创建时间'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('blog_article',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='文章编号ID'),
    sa.Column('user_id', sa.Integer(), nullable=True, comment='作者id'),
    sa.Column('catalog_id', sa.Integer(), nullable=True, comment='所属目录'),
    sa.Column('title', sa.String(length=50), nullable=True, comment='标题'),
    sa.Column('content', sa.Text(), nullable=True, comment='内容'),
    sa.Column('status', sa.Integer(), nullable=True, comment='状态(1发布,0不发布)'),
    sa.Column('create_at', sa.DateTime(), nullable=True, comment='创建时间'),
    sa.Column('update_at', sa.DateTime(), nullable=True, comment='修改时间'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('blog_catalog',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='目录编号ID'),
    sa.Column('parent_id', sa.Integer(), nullable=True, comment='父目录'),
    sa.Column('user_id', sa.Integer(), nullable=True, comment='用户id'),
    sa.Column('name', sa.String(length=50), nullable=True, comment='名称'),
    sa.Column('sort', sa.Integer(), nullable=True, comment='排序'),
    sa.Column('status', sa.Integer(), nullable=True, comment='状态(1发布,0不发布)'),
    sa.Column('create_at', sa.DateTime(), nullable=True, comment='创建时间'),
    sa.Column('update_at', sa.DateTime(), nullable=True, comment='修改时间'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('blog_tag',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='标签编号ID'),
    sa.Column('user_id', sa.Integer(), nullable=True, comment='所属用户id'),
    sa.Column('name', sa.Integer(), nullable=True, comment='标签名字'),
    sa.Column('status', sa.Integer(), nullable=True, comment='状态(1发布,0不发布)'),
    sa.Column('create_at', sa.DateTime(), nullable=True, comment='创建时间'),
    sa.Column('update_at', sa.DateTime(), nullable=True, comment='修改时间'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('blog_article_tag',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False, comment='标识'),
    sa.Column('article_id', sa.Integer(), nullable=True, comment='文章编号'),
    sa.Column('tag_id', sa.Integer(), nullable=True, comment='标签编号'),
    sa.ForeignKeyConstraint(['article_id'], ['blog_article.id'], ),
    sa.ForeignKeyConstraint(['tag_id'], ['blog_tag.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('blog_article_tag')
    op.drop_table('blog_tag')
    op.drop_table('blog_catalog')
    op.drop_table('blog_article')
    op.drop_table('admin_mail')
    # ### end Alembic commands ###
